// mmc_can.h : main header file for the mmc_can DLL
//

#pragma once

#include <windows.h>
#include <afxwin.h>
#include "vcom/vcom.h"

#define WM_CAN_CHANGE_ENGINE_TEMP (WM_USER+64)
#define WM_CAN_CHANGE_FUEL (WM_USER+65)
#define WM_CAN_CHANGE_GEARBOX_TEMP (WM_USER+66)
#define WM_CAN_CHANGE_AIR_TEMP (WM_USER+67)
#define WM_CAN_CHANGE_BAT_CHARGE_FLAG (WM_USER+68)
#define WM_CAN_CHANGE_WARN_ENG_TEMP_FLAG (WM_USER+69)
#define WM_CAN_CHANGE_WARN_OIL_PRES_FLAG (WM_USER+70)
#define WM_CAN_CHANGE_CHECK_ENGINE_FLAG (WM_USER+71)
#define WM_CAN_CHANGE_ACTIVE (WM_USER+72)
#define WM_CAN_CHANGE_SPEED (WM_USER+73)
#define WM_CAN_CHANGE_SAFETY_BELT_FLAG (WM_USER+74)
#define WM_CAN_CHANGE_ECU_ACTIVE (WM_USER+75)
#define WM_CAN_CHANGE_SRS_ACTIVE (WM_USER+76)
#define WM_CAN_CHANGE_BREAK_SWITCH_STATE (WM_USER+77)
#define WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE (WM_USER+78)
#define WM_CAN_CHANGE_FRONT_WINDOW_HEATING_STATE (WM_USER+79)
#define WM_CAN_CHANGE_LOW_BEAM_STATE (WM_USER+80)
#define WM_CAN_CHANGE_HIGH_BEAM_STATE (WM_USER+81)
#define WM_CAN_CHANGE_DRIVERS_DOOR_STATE (WM_USER+82)
#define WM_CAN_CHANGE_PASSENGERS_DOOR_STATE (WM_USER+83)
#define WM_CAN_CHANGE_REAR_DOORS_STATE (WM_USER+84)
//#define WM_CAN_CHANGE_HOOD_STATE (WM_USER+85)
//#define WM_CAN_CHANGE_TAILGATE_STATE (WM_USER+86)
#define WM_CAN_CHANGE_RIGHT_TURN_STATE (WM_USER+87)
#define WM_CAN_CHANGE_LEFT_TURN_STATE (WM_USER+88)
#define WM_CAN_CHANGE_IN_CAR_TEMP (WM_USER+89)
#define WM_CAN_CHANGE_TARGET_TEMP (WM_USER+90)
#define WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE (WM_USER+91)
#define WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION (WM_USER+92)
#define WM_CAN_CHANGE_AIR_FLOW_DIRECTION (WM_USER+93)
#define WM_CAN_CHANGE_AIR_FLOW_SPEED (WM_USER+94)
#define WM_CAN_CHANGE_RPM (WM_USER+95)
#define WM_CAN_CHANGE_OBD_PID_MSG (WM_USER+96)
#define WM_CAN_CHANGE_CONDITIONER_STATE (WM_USER+97)
#define WM_CAN_CHANGE_RECIRCULATION_STATE (WM_USER+98)
#define WM_CAN_CHANGE_WINDSHIELD_FLOW_STATE (WM_USER+99)
#define WM_CAN_READED_OBD2_ERRORS (WM_USER+100)
#define WM_CAN_CHANGE_PARK_STATE (WM_USER+101)
#define WM_CAN_CHANGE_MARKER_LIGHT_STATE (WM_USER+102)
#define WM_CAN_CHANGE_IGNITION_STATE (WM_USER+103)

#define WM_CAN_VERSION_READED (WM_USER+109)
#define WM_CAN_CHANGE_GPS_MSG (WM_USER+110)
#define WM_CAN_SENDED_GPS_MSG (WM_USER+111)

#define WM_CAN_CHANGE_IR  _T("WM_CAN_CHANGE_IR_1234567890")

#define MAX_OBD2_CODES     120
#define NMEA_VCOM_PORT_NUM 5
#define NMEA_DATA_MAX_LEN  VCOMDRIVER_WRITE_SIZE
// Cmmc_canApp
// See mmc_can.cpp for the implementation of this class
//
extern "C" __declspec(dllexport) int Init(HWND m_MsgHWND);
extern "C" __declspec(dllexport) int SetSleepTime(int value);
extern "C" __declspec(dllexport) int GetEngineTemp(); // 1 = 1C
extern "C" __declspec(dllexport) int GetFuel(); // 1 = 0.1L
extern "C" __declspec(dllexport) int GetGearboxTemp(); // 1 = 1C
extern "C" __declspec(dllexport) int GetAirTemp(); // 1 = 1C
extern "C" __declspec(dllexport) int GetSpeed(); // 1 = 0.01 km/h
extern "C" __declspec(dllexport) int GetRPM(); // 1 = 1 RPM
extern "C" __declspec(dllexport) int GetBatteryChargeFlag();
extern "C" __declspec(dllexport) int GetWarningEngineTempFlag();
extern "C" __declspec(dllexport) int GetWarningOilPressureFlag();
extern "C" __declspec(dllexport) int GetCheckEngine();
extern "C" __declspec(dllexport) int GetActiveCAN();
extern "C" __declspec(dllexport) int ResetECUErrors();
extern "C" __declspec(dllexport) int ResetEngineECU(int mode);
extern "C" __declspec(dllexport) int Fan1On();
extern "C" __declspec(dllexport) int Fan1Off();
extern "C" __declspec(dllexport) int Fan2On();
extern "C" __declspec(dllexport) int Fan2Off();
extern "C" __declspec(dllexport) int SetIdleRPM(int rpm);
extern "C" __declspec(dllexport) int GetSafetyBeltFlag();
extern "C" __declspec(dllexport) int GetActiveECU();
extern "C" __declspec(dllexport) int GetActiveECUFuelConsumption();
extern "C" __declspec(dllexport) int GetActiveSRS();
extern "C" __declspec(dllexport) int GetTripDeciMeter();
extern "C" __declspec(dllexport) int GetConsumption10ML();
extern "C" __declspec(dllexport) int SetTripDeciMeter(int value);
extern "C" __declspec(dllexport) int SetConsumption10ML(int value);
extern "C" __declspec(dllexport) int GetBreakSwitchState();
extern "C" __declspec(dllexport) int GetRearWindowHeatingState();
extern "C" __declspec(dllexport) int GetFrontWindowHeatingState();
extern "C" __declspec(dllexport) int GetLowBeamLightState();
extern "C" __declspec(dllexport) int GetHighBeamLightState();
extern "C" __declspec(dllexport) int GetDriversDoorState();
extern "C" __declspec(dllexport) int GetPassengersDoorState();
extern "C" __declspec(dllexport) int GetRearDoorsState();
//extern "C" __declspec(dllexport) int GetHoodState();
//extern "C" __declspec(dllexport) int GetTailgateState();
extern "C" __declspec(dllexport) int GetRightTurnState();
extern "C" __declspec(dllexport) int GetLeftTurnState();
extern "C" __declspec(dllexport) int GetInCarTemp(); // 1 = 1C
extern "C" __declspec(dllexport) int GetTargetTemp(); // 1 = 1C
extern "C" __declspec(dllexport) int GetInstConsumption(); // 1 = 0.002145 L/h
extern "C" __declspec(dllexport) int GetAirFlowDirection(); // 
extern "C" __declspec(dllexport) int GetAirFlowSpeed(); // 
extern "C" __declspec(dllexport) int GetOBD2ErrorCodes(int n);
extern "C" __declspec(dllexport) char* GetOBD2ErrorCodeString(int n);
extern "C" __declspec(dllexport) int GetOBD2PID(int pid);
extern "C" __declspec(dllexport) char* GetGPSMessage();
extern "C" __declspec(dllexport) char* GetCANVersion();
extern "C" __declspec(dllexport) int GetConditionerState();
extern "C" __declspec(dllexport) int GetRecirculationState();
extern "C" __declspec(dllexport) int GetWindshieldFlowState();
extern "C" __declspec(dllexport) int GetParkState();
extern "C" __declspec(dllexport) int GetMarkerLightState();
extern "C" __declspec(dllexport) int GetIgnitionState();
extern "C" __declspec(dllexport) int ResetABSErrors();
extern "C" __declspec(dllexport) int ResetSRSErrors();
extern "C" __declspec(dllexport) int ResetTCUErrors();
extern "C" __declspec(dllexport) int TesterPresent();
extern "C" __declspec(dllexport) int ReadCANVersion();
extern "C" __declspec(dllexport) int ReadOBD2ErrorCodes(int mode);
extern "C" __declspec(dllexport) int ReadOBD2PID(int pid);
extern "C" __declspec(dllexport) int SendGPSMessageA(char* msg);
extern "C" __declspec(dllexport) int SendGPSMessageW(WCHAR* msg);


class Cmmc_canApp : public CWinApp
{
public:
	Cmmc_canApp();

// Overrides
public:
	virtual BOOL InitInstance();
	virtual BOOL ExitInstance();
  BOOL OpenPort();
  BOOL PortRead();
  BOOL PortWrite();
  BOOL BufAnalize();
  void ECUMsgAnalize(unsigned char* m_msg);
  void ClosePort();

public:
  int m_inited;
  HANDLE hPort;
  unsigned long m_sleep_time;
  char m_can_version[8];
  signed long m_enginetemp;
  signed long m_gearboxtemp;
  signed long m_airtemp;
  unsigned long m_speed_10mph;
  unsigned long m_rpm;
  unsigned long m_speed_10mph_last;
  unsigned long m_speed_last_counter_mark;
  unsigned long m_consumption_counter_10ml;
  unsigned long m_cur_consumption;
  unsigned long m_cur_consumption_last;
  unsigned long m_trip_dm;
  float m_fuel;
  long m_batterycharge;
  long m_checkengine;
  long m_canactive;
  unsigned long m_canactivemark;
  long m_lowoilpressure;
  long m_highenginetemperature;
  long m_safetybelt;
  long m_ecuactive;
  unsigned long m_ecuactivemark;
  long m_ecuactive_fuelconsumption;
  unsigned long m_fuelconsumptionactivemark;
  unsigned char m_ecu_msg_counter;
  long m_srsactive;
  unsigned long m_srsactivemark;
  long m_break_switch_on;
  long m_rear_window_heating;
  long m_front_window_heating;
  long m_low_beam_light;
  long m_high_beam_light;
  long m_drivers_door_opened;
  long m_passangers_door_opened;
  long m_rear_doors_opened;
  //long m_hood_opened;
  //long m_tailgate_opened;
  long m_right_turn_light;
  long m_left_turn_light;
  signed long m_incartemp;
  signed long m_targettemp;
  long m_climatic_on;
  unsigned long m_inst_consumption;
  unsigned long m_air_flow_direction;
  unsigned long m_air_flow_speed;
  long m_conditioner_state;
  long m_recirculation_state;
  long m_windshield_flow_state;
  long m_park_state;
  long m_marker_light_state;
  long m_ignition_state;
  long m_ignition_state_last;
  unsigned long m_dashboard_activemark;
  long m_dashboard_active;
  long m_2DE_4_byte;

  unsigned int m_obd2_error_code[MAX_OBD2_CODES];
  unsigned int m_obd2_error_code_num;
  unsigned char m_obd2_error_code_msg[256];
  unsigned char m_obd2_error_code_msg_offset;
  unsigned short m_obd2_error_code_msg_length;
  unsigned char m_obd2_error_code_msg_num;
  char m_obd2_error_string[6];
  unsigned long m_obd2_pid_value[256];

  unsigned short m_last_gps_id;
  unsigned char m_gps_msg_temp[256];
  unsigned long m_gps_msg_temp_ptr;
  unsigned char m_gps_msg[256];
  unsigned long m_gps_msg_len;
  unsigned char m_gps_tx_msg[256];
  unsigned long m_gps_tx_msg_len;

  UINT TimerId;
  UINT ir_Message;

  HWND m_HWND_dest;

  DCB m_dcb;
  DWORD m_tick_counter_mark;
  //DWORD m_last_buf_len;
  unsigned char m_com_buf[512];
  unsigned char m_output_buf[512];
  DWORD m_output_buf_len;
  BOOL m_comsending;
  BOOL m_resetting_ecu;
  BOOL m_fan_switching_on;
  BOOL m_fan_switching_off;
  BOOL m_fan2_switching_on;
  BOOL m_fan2_switching_off;
  BOOL m_setting_idle;
  BOOL m_clearing_abs_error;
  BOOL m_clearing_srs_error;
  BOOL m_clearing_tcu_error;
  BOOL m_reading_errors_obd2;
  BOOL m_reading_pid_obd2;
  DWORD m_resetting_ecu_mark;
  DWORD m_fan_switching_mark;
  DWORD m_fan2_switching_mark;
  DWORD m_setting_idle_mark;
  DWORD m_clearing_abs_error_mark;
  DWORD m_clearing_srs_error_mark;
  DWORD m_clearing_tcu_error_mark;
  DWORD m_reading_errors_obd2_mark;
  DWORD m_reading_pid_obd2_mark;
  unsigned char m_reset_ecu_mode;
  unsigned char m_rpm_value;

  DWORD DllThreadID;
  HANDLE DllThread; //thread's handle

  bool vcom_inited;
  CRITICAL_SECTION m_nmea_data_operation;
  unsigned int m_nmea_data_len;
  BYTE m_nmea_data[NMEA_DATA_MAX_LEN];

  DECLARE_MESSAGE_MAP()
};

DWORD WINAPI ThreadProc(__in  LPVOID lpParameter);
void CALLBACK TimerProc(HWND hWnd, UINT nMsg, UINT nIDEvent, DWORD dwTime);

