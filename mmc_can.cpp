// mmc_can.cpp : Defines the initialization routines for the DLL.
//

#include "mmc_can.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//


// Cmmc_canApp

BEGIN_MESSAGE_MAP(Cmmc_canApp, CWinApp)
END_MESSAGE_MAP()


// Cmmc_canApp construction

Cmmc_canApp::Cmmc_canApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Cmmc_canApp object

Cmmc_canApp theApp;


// Cmmc_canApp initialization

BOOL Cmmc_canApp::InitInstance()
{
	CWinApp::InitInstance();

  unsigned int uiMessage;

  vcom_inited = true;

  if (NMEA_VCOM_PORT_NUM != VCOM_Init(NMEA_VCOM_PORT_NUM)) {
	vcom_inited = false;
	AfxMessageBox(L"VCOM_fail", MB_ICONWARNING);
  };

  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_ENGINE_TEMP"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_FUEL"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_GEARBOX_TEMP"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_AIR_TEMP"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_BAT_CHARGE_FLAG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_WARN_ENG_TEMP_FLAG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_WARN_OIL_PRES_FLAG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_CHECK_ENGINE_FLAG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_ACTIVE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_SPEED"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_SAFETY_BELT_FLAG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_ECU_ACTIVE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_SRS_ACTIVE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_BREAK_SWITCH_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_FRONT_WINDOW_HEATING_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_LOW_BEAM_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_HIGH_BEAM_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_DRIVERS_DOOR_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_PASSENGERS_DOOR_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_REAR_DOORS_STATE"));
//  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_HOOD_STATE"));
//  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_TAILGATE_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_RIGHT_TURN_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_LEFT_TURN_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_IN_CAR_TEMP"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_TARGET_TEMP"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_AIR_FLOW_DIRECTION"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_AIR_FLOW_SPEED"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_RPM"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_OBD_PID_MSG"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_CONDITIONER_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_RECIRCULATION_STATE"));
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_WINDSHIELD_FLOW_STATE"));

  uiMessage = RegisterWindowMessage(_T("WM_CAN_READED_OBD2_ERRORS"));  

  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_PARK_STATE"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_MARKER_LIGHT_STATE"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_IGNITION_STATE"));  

  uiMessage = RegisterWindowMessage(_T("WM_CAN_VERSION_READED"));  

  uiMessage = RegisterWindowMessage(_T("WM_CAN_CHANGE_GPS_MSG"));  
  uiMessage = RegisterWindowMessage(_T("WM_CAN_SENDED_GPS_MSG"));  

  ir_Message = RegisterWindowMessage(WM_CAN_CHANGE_IR);
  m_canactive = 0;
  m_ecuactive = 0;
  m_srsactive = 0;

  m_trip_dm = 0;
  m_cur_consumption = 0;

  hPort = NULL;
  m_HWND_dest = HWND_BROADCAST;

  m_enginetemp = -1000;
  m_gearboxtemp = -1000;
  m_airtemp = -1000;
  m_speed_10mph = 0;
  m_consumption_counter_10ml = 0;
  m_fuel = -1000;

  m_gps_msg_temp_ptr = 1000;

  m_sleep_time = 5;

  m_nmea_data_len = 0;
  memset(&m_nmea_data[m_nmea_data_len], 0x00, VCOMDRIVER_WRITE_SIZE);

  InitializeCriticalSection(&m_nmea_data_operation);

  return TRUE;
}
//*****************************************************************************
BOOL Cmmc_canApp::ExitInstance()
{
  ClosePort();
  VCOM_DeInit(NMEA_VCOM_PORT_NUM);
  if(TimerId) KillTimer(NULL,TimerId);
  CloseHandle(theApp.DllThread);

  CWinApp::ExitInstance();

	return TRUE;
}
//*****************************************************************************
BOOL Cmmc_canApp::OpenPort()
{
  CString m_comname;
  DWORD m_dwerrors;
  BOOL fSuccess;
  COMMTIMEOUTS m_PortTimeouts;
  OVERLAPPED m_over = {0};
  COMSTAT m_comstat;
  COMMPROP m_comprop;

  m_comname = _T("COM3:");
  if(hPort)
  {
    CloseHandle(hPort);
    hPort = NULL;
  }
  if(!hPort)
  {
    hPort = ::CreateFile(m_comname, 
                         GENERIC_READ | GENERIC_WRITE, 
                         0, 
                         NULL, 
                         OPEN_EXISTING, 
                         0, // ���������� ������
                         NULL);
    if (hPort == INVALID_HANDLE_VALUE)
    {
      m_dwerrors = GetLastError();
      m_comname.Format(_T("COM3: ,Error:%d"),m_dwerrors);
      AfxMessageBox(m_comname,MB_ICONSTOP);
      hPort = NULL;
      return FALSE;
    }
  }
  m_dcb.DCBlength = sizeof(DCB);
  fSuccess = GetCommProperties(hPort,&m_comprop);
  if(!fSuccess)
  {
    CloseHandle(hPort);
    hPort = NULL;
    return FALSE;
  }
  GetCommState(hPort, &m_dcb);
  EscapeCommFunction(hPort,CLRRTS);
  EscapeCommFunction(hPort,CLRDTR);
  m_dcb.BaudRate = CBR_115200;     //  baud rate
  m_dcb.ByteSize = 8;             //  data size, xmit and rcv
  m_dcb.Parity   = NOPARITY;      //  parity bit
  m_dcb.StopBits = ONESTOPBIT;    //  stop bit
  m_dcb.fDtrControl = DTR_CONTROL_DISABLE;
  m_dcb.fRtsControl = RTS_CONTROL_DISABLE;
  m_dcb.fBinary = TRUE;
  m_dcb.fOutxCtsFlow = FALSE;
  m_dcb.fOutxDsrFlow = FALSE;
  m_dcb.fDsrSensitivity = FALSE;
  m_dcb.fTXContinueOnXoff = FALSE;
  m_dcb.fOutX = FALSE;
  m_dcb.fInX = FALSE;
  m_dcb.fErrorChar = FALSE;
  m_dcb.fNull = FALSE;
  m_dcb.fAbortOnError = FALSE;
  m_dcb.XoffLim = 4096;
  m_dcb.XoffLim = 4096;
  m_dcb.XonChar = 0;
  m_dcb.XoffChar = 0;
  m_dcb.ErrorChar = 0;
  m_dcb.EofChar = 0;
  m_dcb.EvtChar = 0;
  fSuccess = SetCommState(hPort, &m_dcb);
  if(!fSuccess)
  {
    m_comname.Format(_T("������: �� ������� ��������� ���� COM3 � �����������:\n%i,%i,%i,%i"),
                     m_dcb.BaudRate,
                     m_dcb.ByteSize,
                     m_dcb.Parity,
                     m_dcb.StopBits);
    AfxMessageBox(m_comname,MB_ICONSTOP);
    CloseHandle(hPort);
    hPort = NULL;
    return FALSE;
  }
  if(m_comprop.dwMaxRxQueue == 0) m_comprop.dwMaxRxQueue = 4096;
  if(m_comprop.dwMaxTxQueue == 0) m_comprop.dwMaxTxQueue = 4096;
  SetupComm(hPort, m_comprop.dwMaxRxQueue, m_comprop.dwMaxTxQueue); // ��������� ������ ������ � ��������
  m_PortTimeouts.ReadIntervalTimeout = MAXDWORD;
  m_PortTimeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
  m_PortTimeouts.ReadTotalTimeoutConstant = 2;      // 2 �� �������
  m_PortTimeouts.WriteTotalTimeoutMultiplier = 0;
  m_PortTimeouts.WriteTotalTimeoutConstant = 0;
  SetCommTimeouts(hPort, &m_PortTimeouts); // ��������� ���������
  ClearCommBreak(hPort);
  PurgeComm(hPort,PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR); // ������� �������
  ClearCommError(hPort, &m_dwerrors, &m_comstat);

	return TRUE;
}
//*****************************************************************************
BOOL Cmmc_canApp::PortRead()
{
  BOOL fSuccess = FALSE;
  DWORD i;
  DWORD len = 0;
  DWORD num;
  DWORD m_errors = 0;
  COMSTAT m_comstat;
  OVERLAPPED m_over = {0};
  unsigned char m_buf[512];

  if(!hPort)
  {
    return FALSE;
  }
  fSuccess = ClearCommError(hPort, &m_errors, &m_comstat);  // ������ ���������� ���� � ������ ������
  num = m_comstat.cbInQue;
  if(num == 0) return FALSE;
  if(num >= 1)
  {
    if(num > 512) num = 512;
    len = 0;
    fSuccess = ReadFile(hPort, m_buf, num, &len, &m_over);  // ������ �����
    if(len)
    {
      num = sizeof(m_com_buf);
      for(i=0; i < (num - len); i++) // �������� ������ �����
      {
        m_com_buf[i] = m_com_buf[i + len];
      }
      for(i=0; i < len; i++) // ��������� �������� ����� � �����
      {
        m_com_buf[i + num - len] = m_buf[i];
      }
      BufAnalize();
    }
  }
  return TRUE;
}
//*****************************************************************************
BOOL Cmmc_canApp::PortWrite()
{
  BOOL fSuccess = FALSE;
  DWORD len = 0;
  DWORD num;
  DWORD m_errors = 0;
  COMSTAT m_comstat;
  OVERLAPPED m_over = {0};
  CString m_str;
  CString m_string;

  if(!hPort) return FALSE;
  if(m_comsending)
  {
    fSuccess = ClearCommError(hPort, &m_errors, &m_comstat);  // ������ ���������� ���� � ������
    num = m_comstat.cbOutQue;
    if(num == 0)
    {
      m_comsending = FALSE;
      return TRUE;
    }
  }
  else
  {
    if(m_output_buf_len)
    {
      num = m_output_buf_len;
      if(num > 512) num = 512;
      fSuccess = WriteFile(hPort, m_output_buf, num, &len, &m_over);  // ���������� �����
      if(fSuccess)
      {
        m_output_buf_len -= len;
        m_comsending = TRUE;
      }
      else
      {
      }
      return FALSE;
    }
  }
  return TRUE;
}

//*****************************************************************************
BOOL Cmmc_canApp::BufAnalize()
{
  DWORD i;
  DWORD j;
  DWORD num;
  unsigned long templong;

  if(m_canactive == 0)
  {
    m_canactive = 1;
    PostMessage(m_HWND_dest,WM_CAN_CHANGE_ACTIVE,(WPARAM)m_canactive,0);
  }
  m_canactivemark = GetTickCount();
  num = sizeof(m_com_buf);
  for(i=0; i < (num - 16); i++) // 
  {
    if((m_com_buf[i] == 0) && (m_com_buf[i+1] == 0) &&
       (m_com_buf[i+2] == 0) && (m_com_buf[i+3] == 0))
    {
	    // ID 281
      if((m_com_buf[i+4] == 0x02) && (m_com_buf[i+5] == 0x81))
      {
        PostMessage(m_HWND_dest,ir_Message,(WPARAM)m_com_buf[i+13],0);
        memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 551
      else if((m_com_buf[i+4] == 0x05) && (m_com_buf[i+5] == 0x51)) 
      {
        if(m_ecuactive == 0)
        {
          if(m_ecu_msg_counter != m_com_buf[i+11])
          {
            m_ecu_msg_counter = m_com_buf[i+11];
            m_ecuactive = 1;
            PostMessage(m_HWND_dest,WM_CAN_CHANGE_ECU_ACTIVE,(WPARAM)m_ecuactive,0);
          }
        }
        m_ecuactivemark = GetTickCount();
        if(m_enginetemp != ((signed long)m_com_buf[i+7] - 40))
        {
          m_enginetemp = (signed long)m_com_buf[i+7] - 40;
          //PostMessage(m_HWND_dest,WM_CAN_CHANGE_ENGINE_TEMP,(WPARAM)m_enginetemp,0);
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_ENGINE_TEMP,(WPARAM)m_com_buf[i+7],0);
        }
        if(m_batterycharge != ((long)m_com_buf[i+9] & 0x01))
        {
          m_batterycharge = (long)m_com_buf[i+9] & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_BAT_CHARGE_FLAG,(WPARAM)m_batterycharge,0);
        }
        if(m_checkengine != ((long)m_com_buf[i+10] & 0x03))
        {
          m_checkengine = (long)m_com_buf[i+10] & 0x03;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_CHECK_ENGINE_FLAG,(WPARAM)m_checkengine,0);
        }
        if(m_lowoilpressure != (((long)m_com_buf[i+10] >> 2) & 0x01))
        {
          m_lowoilpressure = ((long)m_com_buf[i+10] >> 2) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_WARN_OIL_PRES_FLAG,(WPARAM)m_lowoilpressure,0);
        }
        if(m_highenginetemperature != (((long)m_com_buf[i+10] >> 3) & 0x01))
        {
          m_highenginetemperature = ((long)m_com_buf[i+10] >> 3) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_WARN_ENG_TEMP_FLAG,(WPARAM)m_highenginetemperature,0);
        }
        m_cur_consumption = (long)m_com_buf[i+8];
		    if(m_cur_consumption_last != m_cur_consumption)
		    {
          if(m_ecuactive_fuelconsumption == 0)
          {
            m_ecuactive_fuelconsumption = 1;
            PostMessage(m_HWND_dest,WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE,(WPARAM)m_ecuactive_fuelconsumption,0);
          }
          m_fuelconsumptionactivemark = GetTickCount();
			    if(m_cur_consumption_last > m_cur_consumption) m_consumption_counter_10ml++;
			    m_cur_consumption_last = m_cur_consumption;
		    }
        templong = (unsigned long)m_com_buf[i+13];
        templong |= (unsigned long)m_com_buf[i+12] << 8;
        if(m_inst_consumption != templong)
        {
          m_inst_consumption = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_FUEL_INSTANT_CONSUMPTION,(WPARAM)m_inst_consumption,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 280
      else if((m_com_buf[i+4] == 0x02) && (m_com_buf[i+5] == 0x80)) 
      {
        if(m_fuel != ((float)m_com_buf[i+7] / 2))
        {
          m_fuel = (float)m_com_buf[i+7] / 2;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_FUEL,(WPARAM)(m_fuel * 10),0);
        }
        if(m_airtemp != ((signed long)m_com_buf[i+8] - 40))
        {
          m_airtemp = (signed long)m_com_buf[i+8] - 40;
          //PostMessage(m_HWND_dest,WM_CAN_CHANGE_AIR_TEMP,(WPARAM)m_airtemp,0);
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_AIR_TEMP,(WPARAM)m_com_buf[i+8],0);
        }
        templong = (unsigned long)m_com_buf[i+11];
        templong |= (unsigned long)m_com_buf[i+10] << 8;
        if(m_speed_10mph != templong)
        {
          m_speed_10mph = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_SPEED,(WPARAM)m_speed_10mph,0);
        }
        m_dashboard_active = 1;
        m_dashboard_activemark = GetTickCount();
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 560
      else if((m_com_buf[i+4] == 0x05) && (m_com_buf[i+5] == 0x60)) 
      {
        if(m_gearboxtemp != ((signed long)m_com_buf[i+6] - 40))
        {
          m_gearboxtemp = (signed long)m_com_buf[i+6] - 40;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_GEARBOX_TEMP,(WPARAM)m_com_buf[i+6],0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 498
      else if((m_com_buf[i+4] == 0x04) && (m_com_buf[i+5] == 0x98)) 
      {
        if(m_srsactive == 0)
        {
          m_srsactive = 1;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_SRS_ACTIVE,(WPARAM)m_srsactive,0);
        }
        m_srsactivemark = GetTickCount();
        if(m_safetybelt != ((long)m_com_buf[i+6] & 0x03))
        {
          m_safetybelt = (long)m_com_buf[i+6] & 0x03;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_SAFETY_BELT_FLAG,(WPARAM)m_safetybelt,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 354 ABS
      else if((m_com_buf[i+4] == 0x03) && (m_com_buf[i+5] == 0x54)) 
      {
        if(m_break_switch_on != ((long)m_com_buf[i+6] & 0x01))
        {
          m_break_switch_on = (long)m_com_buf[i+6] & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_BREAK_SWITCH_STATE,(WPARAM)m_break_switch_on,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 481 BCM
      else if((m_com_buf[i+4] == 0x04) && (m_com_buf[i+5] == 0x81)) 
      {
        if(m_rear_window_heating != (((long)m_com_buf[i+9] >> 1) & 0x01))
        {
          m_rear_window_heating = ((long)m_com_buf[i+9] >> 1) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_REAR_WINDOW_HEATING_STATE,(WPARAM)m_rear_window_heating,0);
        }
        if(m_front_window_heating != (((long)m_com_buf[i+9] >> 2) & 0x01))
        {
          m_front_window_heating = ((long)m_com_buf[i+9] >> 2) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_FRONT_WINDOW_HEATING_STATE,(WPARAM)m_front_window_heating,0);
        }
        if(m_low_beam_light != (((long)m_com_buf[i+6] >> 6) & 0x01))
        {
          m_low_beam_light = ((long)m_com_buf[i+6] >> 6) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_LOW_BEAM_STATE,(WPARAM)m_low_beam_light,0);
        }
        if(m_high_beam_light != (((long)m_com_buf[i+6] >> 4) & 0x01))
        {
          m_high_beam_light = ((long)m_com_buf[i+6] >> 4) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_HIGH_BEAM_STATE,(WPARAM)m_high_beam_light,0);
        }
        if(m_drivers_door_opened != (((long)m_com_buf[i+6] >> 7) & 0x01))
        {
          m_drivers_door_opened = ((long)m_com_buf[i+6] >> 7) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_DRIVERS_DOOR_STATE,(WPARAM)m_drivers_door_opened,0);
        }
        if(m_passangers_door_opened != (((long)m_com_buf[i+6] >> 5) & 0x01))
        {
          m_passangers_door_opened = ((long)m_com_buf[i+6] >> 5) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_PASSENGERS_DOOR_STATE,(WPARAM)m_passangers_door_opened,0);
        }
        if(m_rear_doors_opened != (((long)m_com_buf[i+7] >> 4) & 0x01))
        {
          m_rear_doors_opened = ((long)m_com_buf[i+7] >> 4) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_REAR_DOORS_STATE,(WPARAM)m_rear_doors_opened,0);
        }
        if(m_right_turn_light != (((long)m_com_buf[i+8] >> 0) & 0x01))
        {
          m_right_turn_light = ((long)m_com_buf[i+8] >> 0) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_RIGHT_TURN_STATE,(WPARAM)m_right_turn_light,0);
        }
        if(m_left_turn_light != (((long)m_com_buf[i+8] >> 1) & 0x01))
        {
          m_left_turn_light = ((long)m_com_buf[i+8] >> 1) & 0x01;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_LEFT_TURN_STATE,(WPARAM)m_left_turn_light,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 555 Climatic
      else if((m_com_buf[i+4] == 0x05) && (m_com_buf[i+5] == 0x55)) 
      {
        if(m_incartemp != ((signed long)m_com_buf[i+6] - 40))
        {
          m_incartemp = (signed long)m_com_buf[i+6] - 40;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_IN_CAR_TEMP,(WPARAM)m_com_buf[i+6],0);
        }
        if(m_targettemp != (signed long)(m_com_buf[i+7] & 0x1F))
        {
          m_targettemp = (signed long)(m_com_buf[i+7] & 0x1F);
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_TARGET_TEMP,(WPARAM)(m_targettemp + 13),0);
        }
        templong = (unsigned long)((m_com_buf[i+7] & 0xE0) >> 5);
        if(m_air_flow_direction != templong)
        {
          m_air_flow_direction = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_AIR_FLOW_DIRECTION,(WPARAM)m_air_flow_direction,0);
        }
        templong = (unsigned long)(m_com_buf[i+8] & 0x07);
        if(m_air_flow_speed != templong)
        {
          m_air_flow_speed = templong;
          templong = (unsigned long)(m_com_buf[i+8] | (m_com_buf[i+9] << 8));
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_AIR_FLOW_SPEED,(WPARAM)m_air_flow_speed,(LPARAM)templong);
        }
        templong = (unsigned long)((m_com_buf[i+8] & 0x08) >> 3);
        if(m_conditioner_state != templong)
        {
          m_conditioner_state = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_CONDITIONER_STATE,(WPARAM)m_conditioner_state,0);
        }
        templong = (unsigned long)((m_com_buf[i+8] & 0x10) >> 4);
        if(m_recirculation_state != templong)
        {
          m_recirculation_state = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_RECIRCULATION_STATE,(WPARAM)m_recirculation_state,0);
        }
        templong = (unsigned long)((m_com_buf[i+9] & 0x08) >> 3);
        if(m_windshield_flow_state != templong)
        {
          m_windshield_flow_state = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_WINDSHIELD_FLOW_STATE,(WPARAM)m_windshield_flow_state,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 2DE
      else if((m_com_buf[i+4] == 0x02) && (m_com_buf[i+5] == 0xDE)) 
      {
        templong = (long)(m_com_buf[i+8] & 0x01);
        if(m_park_state != templong)
        {
          m_park_state = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_PARK_STATE,(WPARAM)m_park_state,0);
        }
        templong = (long)((m_com_buf[i+8] & 0x04) >> 2);
        if(m_marker_light_state != templong)
        {
          m_marker_light_state = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_MARKER_LIGHT_STATE,(WPARAM)m_marker_light_state,0);
        }
        templong = (long)(m_com_buf[i+9]);
        if(m_2DE_4_byte != templong)
        {
          m_2DE_4_byte = templong;
        }
        m_dashboard_active = 1;
        m_dashboard_activemark = GetTickCount();
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 7E8 ECU
      else if((m_com_buf[i+4] == 0x07) && (m_com_buf[i+5] == 0xE8)) 
      {
        ECUMsgAnalize(&m_com_buf[i+6]);
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 4Ax GPS
      else if(vcom_inited && (m_com_buf[i+4] == 0x04) && ((m_com_buf[i+5] == 0xA4) ||
                                           (m_com_buf[i+5] == 0xA6) ||
                                           (m_com_buf[i+5] == 0xA8) ||
                                           (m_com_buf[i+5] == 0xAA) ||
                                           (m_com_buf[i+5] == 0xAC) ||
                                           (m_com_buf[i+5] == 0xAE))) 
      {
		EnterCriticalSection(&m_nmea_data_operation);
		if (m_nmea_data_len+8 <= NMEA_DATA_MAX_LEN) {
			memcpy(&m_nmea_data[m_nmea_data_len], &m_com_buf[i+6], 8);
			m_nmea_data_len+=8;
		}
		else {
			m_nmea_data_len = 0;
			memcpy(&m_nmea_data[m_nmea_data_len], 0x00, NMEA_DATA_MAX_LEN);
		}
		LeaveCriticalSection(&m_nmea_data_operation);
		memset(&m_com_buf[i], 0xFF, 16);
      }
      // ID 180 ECU RPM
      else if((m_com_buf[i+4] == 0x01) && (m_com_buf[i+5] == 0x80)) 
      {
        templong = (unsigned long)m_com_buf[i+7];
        templong |= (unsigned long)m_com_buf[i+6] << 8;
        templong = templong / 8;
        if(templong != m_rpm)
        {
          m_rpm = templong;
          PostMessage(m_HWND_dest,WM_CAN_CHANGE_RPM,(WPARAM)m_rpm,0);
        }
		memset(&m_com_buf[i], 0xFF, 16);
      }
    }
    else if((m_com_buf[i] == 2) && (m_com_buf[i+1] == 2) &&
            (m_com_buf[i+2] == 0) && (m_com_buf[i+3] == 0) &&
            (m_com_buf[i+4] == 0) && (m_com_buf[i+5] == 0))
    {
      if((m_com_buf[i+6] == 1) && (m_com_buf[i+7] == 1)) // CAN version
      {
        for(j = 0; j < 6; j++) m_can_version[j] = m_com_buf[i + j + 8];
        m_can_version[6] = 0;
        m_can_version[7] = 0;
        PostMessage(m_HWND_dest,WM_CAN_VERSION_READED,0,0);
        memset(&m_com_buf[i], 0xFF, 16);
      }
    }
  }
  return TRUE;
}
//*****************************************************************************
void Cmmc_canApp::ECUMsgAnalize(unsigned char* m_msg)
{
  int m_frametype = (m_msg[0] >> 4) & 0x03;
  if(m_frametype == 0) // Single frame
  {
    if(m_reading_errors_obd2)
    {
      if(m_msg[1] == 0x43) // mode 03
      {
        m_obd2_error_code_num = 0;
        m_obd2_error_code_num = m_msg[2];
        if(m_msg[0] > 2) m_obd2_error_code[0] = (m_msg[3] << 8) | m_msg[4];
        else m_obd2_error_code[0] = 0;
        if(m_msg[0] > 4) m_obd2_error_code[1] = (m_msg[5] << 8) | m_msg[6];
        else m_obd2_error_code[1] = 0;
        m_obd2_error_code[2] = 0;
        PostMessage(m_HWND_dest,WM_CAN_READED_OBD2_ERRORS,(WPARAM)(m_obd2_error_code_num),0);
        m_reading_errors_obd2 = FALSE;
      }
      else if(m_msg[1] == 0x47) // mode 07
      {
        m_obd2_error_code_num = 0;
        m_obd2_error_code_num = m_msg[2];
        if(m_msg[0] > 2) m_obd2_error_code[0] = (m_msg[3] << 8) | m_msg[4];
        else m_obd2_error_code[0] = 0;
        if(m_msg[0] > 4) m_obd2_error_code[1] = (m_msg[5] << 8) | m_msg[6];
        else m_obd2_error_code[1] = 0;
        m_obd2_error_code[2] = 0;
        PostMessage(m_HWND_dest,WM_CAN_READED_OBD2_ERRORS,(WPARAM)(m_obd2_error_code_num),0);
        m_reading_errors_obd2 = FALSE;
      }
    }
    else if(1)//m_reading_pid_obd2)
    {
      if(m_msg[1] == 0x41) // mode 01
      {
        m_obd2_pid_value[m_msg[2]] = 0;
        if(m_msg[0] == 3)
        {
          m_obd2_pid_value[m_msg[2]] = m_msg[3];
        }
        else if(m_msg[0] == 4)
        {
          m_obd2_pid_value[m_msg[2]] = m_msg[4];
          m_obd2_pid_value[m_msg[2]] |= m_msg[3] << 8;
        }
        else if(m_msg[0] == 6)
        {
          m_obd2_pid_value[m_msg[2]] = m_msg[6];
          m_obd2_pid_value[m_msg[2]] |= m_msg[5] << 8;
          m_obd2_pid_value[m_msg[2]] |= m_msg[4] << 16;
          m_obd2_pid_value[m_msg[2]] |= m_msg[3] << 24;
        }
        PostMessage(m_HWND_dest,WM_CAN_CHANGE_OBD_PID_MSG,(WPARAM)(m_msg[2]),(LPARAM)m_obd2_pid_value[m_msg[2]]);
        m_reading_pid_obd2 = FALSE;
      }
    }
  }
  else if(m_frametype == 1) // First frame
  {
    if(m_reading_errors_obd2) m_reading_errors_obd2_mark = GetTickCount();
    if(theApp.m_comsending == FALSE)
    {
      theApp.m_output_buf[0] = 0;
      theApp.m_output_buf[1] = 0;
      theApp.m_output_buf[2] = 0;
      theApp.m_output_buf[3] = 0;
      theApp.m_output_buf[4] = 0x07;
      theApp.m_output_buf[5] = 0xE0;
      theApp.m_output_buf[6] = 0x30; // Flow Control
      theApp.m_output_buf[7] = 0x00; // Block Size
      theApp.m_output_buf[8] = 0x00; // Separation Time
      theApp.m_output_buf[9] = 0x00;
      theApp.m_output_buf[10] = 0x00;
      theApp.m_output_buf[11] = 0x00;
      theApp.m_output_buf[12] = 0x00;
      theApp.m_output_buf[13] = 0x00;
      DWORD m_summ = 0;
      DWORD i;
      for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
      m_summ = 0x10000 - m_summ;
      theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
      theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
      theApp.m_output_buf_len = 16;
      theApp.PortWrite();
      for(i = 0; i < 6; i++)  m_obd2_error_code_msg[i] = m_msg[i + 2];
      m_obd2_error_code_msg_offset = 6;
      m_obd2_error_code_msg_length = m_msg[1] | ((m_msg[0] & 0x0F) << 8);
    }
  }
  else if(m_frametype == 2) // Consecutive frame
  {
    if(m_reading_errors_obd2)
    {
      DWORD i;
      m_reading_errors_obd2_mark = GetTickCount();
      for(i = 0; i < 7; i++)  m_obd2_error_code_msg[i + m_obd2_error_code_msg_offset] = m_msg[i + 1];
      m_obd2_error_code_msg_offset += 7;
      if(m_obd2_error_code_msg_offset >= m_obd2_error_code_msg_length) // finish msg
      {
        m_obd2_error_code_num = m_obd2_error_code_msg[1];
        for(i = 0; i < m_obd2_error_code_num; i++)
          m_obd2_error_code[i] = (m_obd2_error_code_msg[i * 2 + 2] << 8) | m_obd2_error_code_msg[i * 2 + 3];
        PostMessage(m_HWND_dest,WM_CAN_READED_OBD2_ERRORS,(WPARAM)(m_obd2_error_code_num),0);
        m_reading_errors_obd2 = FALSE;
      }
    }
  }
  else if(m_frametype == 3) // Flow Control frame
  {
  }
  else // error
  {
  }
}

//*****************************************************************************
void Cmmc_canApp::ClosePort()
{
  if((hPort != 0) && (hPort != INVALID_HANDLE_VALUE)) CloseHandle(hPort);
}
//*****************************************************************************
DWORD WINAPI ThreadProc(__in  LPVOID lpParameter)
{
  while(1)
  {
    TimerProc(NULL,0,0,0);
    Sleep(theApp.m_sleep_time);
	EnterCriticalSection(&theApp.m_nmea_data_operation);
	::VCOM_Write(NMEA_VCOM_PORT_NUM, theApp.m_nmea_data, theApp.m_nmea_data_len);
	theApp.m_nmea_data_len = 0;
	LeaveCriticalSection(&theApp.m_nmea_data_operation);
  }
  return 0;
}
//*****************************************************************************
void CALLBACK TimerProc(HWND hWnd, UINT nMsg, UINT nIDEvent, DWORD dwTime)
{
  DWORD CurrentTickCount;
  if(theApp.m_comsending) theApp.PortWrite();
  theApp.PortRead();
  CurrentTickCount = GetTickCount();
  if(theApp.m_canactive)
  {
    if((CurrentTickCount - theApp.m_canactivemark) > 2000)
    {
      theApp.m_canactive = 0;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_ACTIVE,(WPARAM)theApp.m_canactive,0);
    }
  }
  if(theApp.m_ecuactive)
  {
    if((CurrentTickCount - theApp.m_ecuactivemark) > 2000)
    {
      theApp.m_ecuactive = 0;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_ECU_ACTIVE,(WPARAM)theApp.m_ecuactive,0);
    }
  }
  if(theApp.m_srsactive)
  {
    if((CurrentTickCount - theApp.m_srsactivemark) > 2000)
    {
      theApp.m_srsactive = 0;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_SRS_ACTIVE,(WPARAM)theApp.m_srsactive,0);
    }
  }
  if(theApp.m_ecuactive_fuelconsumption)
  {
    if((CurrentTickCount - theApp.m_fuelconsumptionactivemark) > 2000)
    {
      theApp.m_ecuactive_fuelconsumption = 0;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_ECU_FUEL_CONSUMPTION_ACTIVE,(WPARAM)theApp.m_ecuactive_fuelconsumption,0);
    }
  }
  if(theApp.m_dashboard_active)
  {
    if((CurrentTickCount - theApp.m_dashboard_activemark) > 2000)
    {
      theApp.m_dashboard_active = 0;
    }
  }
  if(theApp.m_ignition_state)
  {
    if((theApp.m_ecuactive == 0) || (theApp.m_dashboard_active == 0) || (theApp.m_2DE_4_byte == 0))
    {
      theApp.m_ignition_state = 0;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_IGNITION_STATE,(WPARAM)theApp.m_ignition_state,0);
    }
  }
  else // (m_ignition_state == 0)
  {
    if(theApp.m_srsactive)
    {
      theApp.m_ignition_state = 1;
      PostMessage(theApp.m_HWND_dest,WM_CAN_CHANGE_IGNITION_STATE,(WPARAM)theApp.m_ignition_state,0);
    }
  }
  if(theApp.m_resetting_ecu)
  {
    if((CurrentTickCount - theApp.m_resetting_ecu_mark) > 500)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x02; // ���������� ����
        theApp.m_output_buf[7] = 0x11; // ������ �� �����
        theApp.m_output_buf[8] = theApp.m_reset_ecu_mode; // 
        theApp.m_output_buf[9] = 0x00;
        theApp.m_output_buf[10] = 0x00;
        theApp.m_output_buf[11] = 0x00;
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_resetting_ecu = FALSE;
      }
    }
  }
  else if(theApp.m_fan_switching_on)
  {
    if((CurrentTickCount - theApp.m_fan_switching_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x05; // ���������� ����
        theApp.m_output_buf[7] = 0x2F; // �������
        theApp.m_output_buf[8] = 0x00; // 
        theApp.m_output_buf[9] = 0x0A; // ����������
        theApp.m_output_buf[10] = 0x06; // ��� ���������� execute
        theApp.m_output_buf[11] = 0xFF; // ������� On
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_fan_switching_on = FALSE;
      }
    }
  }
  else if(theApp.m_fan_switching_off)
  {
    if((CurrentTickCount - theApp.m_fan_switching_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x05; // ���������� ����
        theApp.m_output_buf[7] = 0x2F; // �������
        theApp.m_output_buf[8] = 0x00; // 
        theApp.m_output_buf[9] = 0x0A; // ����������
        theApp.m_output_buf[10] = 0x00; // ��� ���������� return control to ECU
        theApp.m_output_buf[11] = 0x00; //
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_fan_switching_off = FALSE;
      }
    }
  }
  else if(theApp.m_fan2_switching_on)
  {
    if((CurrentTickCount - theApp.m_fan2_switching_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x05; // ���������� ����
        theApp.m_output_buf[7] = 0x2F; // �������
        theApp.m_output_buf[8] = 0x00; // 
        theApp.m_output_buf[9] = 0x0D; // ����������
        theApp.m_output_buf[10] = 0x06; // ��� ���������� execute
        theApp.m_output_buf[11] = 0xFF; // ������� On
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_fan2_switching_on = FALSE;
      }
    }
  }
  else if(theApp.m_fan2_switching_off)
  {
    if((CurrentTickCount - theApp.m_fan2_switching_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x05; // ���������� ����
        theApp.m_output_buf[7] = 0x2F; // �������
        theApp.m_output_buf[8] = 0x00; // 
        theApp.m_output_buf[9] = 0x0D; // ����������
        theApp.m_output_buf[10] = 0x00; // ��� ���������� return control to ECU
        theApp.m_output_buf[11] = 0x00; //
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_fan2_switching_off = FALSE;
      }
    }
  }
  else if(theApp.m_setting_idle)
  {
    if((CurrentTickCount - theApp.m_setting_idle_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE0;
        theApp.m_output_buf[6] = 0x05; // ���������� ����
        theApp.m_output_buf[7] = 0x2F; // �������
        theApp.m_output_buf[8] = 0x00; // 
        theApp.m_output_buf[9] = 0x42; // ����������
        theApp.m_output_buf[10] = 0x07; // ��� ���������� Adjustment
        theApp.m_output_buf[11] = theApp.m_rpm_value; // �������� � 100 rpm
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_setting_idle = FALSE;
      }
    }
  }
  else if(theApp.m_clearing_abs_error)
  {
    if((CurrentTickCount - theApp.m_clearing_abs_error_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE3;
        theApp.m_output_buf[6] = 0x04; // ���������� ����
        theApp.m_output_buf[7] = 0x14; // �������
        theApp.m_output_buf[8] = 0xFF; // 
        theApp.m_output_buf[9] = 0xFF; // 
        theApp.m_output_buf[10] = 0xFF; // 
        theApp.m_output_buf[11] = 0x00; // 
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_clearing_abs_error = FALSE;
      }
    }
  }
  else if(theApp.m_clearing_srs_error)
  {
    if((CurrentTickCount - theApp.m_clearing_srs_error_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE5;
        theApp.m_output_buf[6] = 0x04; // ���������� ����
        theApp.m_output_buf[7] = 0x14; // �������
        theApp.m_output_buf[8] = 0xFF; // 
        theApp.m_output_buf[9] = 0xFF; // 
        theApp.m_output_buf[10] = 0xFF; // 
        theApp.m_output_buf[11] = 0x00; // 
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_clearing_srs_error = FALSE;
      }
    }
  }
  else if(theApp.m_clearing_tcu_error)
  {
    if((CurrentTickCount - theApp.m_clearing_tcu_error_mark) > 300)
    {
      if(theApp.m_comsending == FALSE)
      {
        theApp.m_output_buf[0] = 0;
        theApp.m_output_buf[1] = 0;
        theApp.m_output_buf[2] = 0;
        theApp.m_output_buf[3] = 0;
        theApp.m_output_buf[4] = 0x07;
        theApp.m_output_buf[5] = 0xE1;
        theApp.m_output_buf[6] = 0x04; // ���������� ����
        theApp.m_output_buf[7] = 0x14; // �������
        theApp.m_output_buf[8] = 0xFF; // 
        theApp.m_output_buf[9] = 0xFF; // 
        theApp.m_output_buf[10] = 0xFF; // 
        theApp.m_output_buf[11] = 0x00; // 
        theApp.m_output_buf[12] = 0x00;
        theApp.m_output_buf[13] = 0x00;
        DWORD m_summ = 0;
        DWORD i;
        for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
        m_summ = 0x10000 - m_summ;
        theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
        theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
        theApp.m_output_buf_len = 16;
        theApp.PortWrite();
        theApp.m_clearing_tcu_error = FALSE;
      }
    }
  }
  else if(theApp.m_reading_errors_obd2)
  {
    if((CurrentTickCount - theApp.m_reading_errors_obd2_mark) > 1000) theApp.m_reading_errors_obd2 = FALSE;
  }
  else if(theApp.m_reading_pid_obd2)
  {
    if((CurrentTickCount - theApp.m_reading_pid_obd2_mark) > 1000) theApp.m_reading_pid_obd2 = FALSE;
  }
  if((CurrentTickCount - theApp.m_speed_last_counter_mark) >= 1000)
  {
    theApp.m_trip_dm += (theApp.m_speed_10mph + theApp.m_speed_10mph_last) *  // speed_10mph * 100 * time_ms / 3600000
                          (CurrentTickCount -theApp. m_speed_last_counter_mark) / 72000;
    //float m_fl;
    //m_fl = (float)(theApp.m_speed_10mph + theApp.m_speed_10mph_last);
    //m_fl *= (float)(CurrentTickCount - theApp.m_speed_last_counter_mark);
    //m_fl /= 72000.0f;
    //theApp.m_trip_dm += (unsigned long)m_fl;
    theApp.m_speed_10mph_last = theApp.m_speed_10mph;
    theApp.m_speed_last_counter_mark = CurrentTickCount;
  }
}
//*****************************************************************************
extern "C" __declspec(dllexport) int Init(HWND m_MsgHWND)
{
  if(theApp.m_inited) return theApp.m_inited;
  theApp.DllThread = CreateThread(NULL,0,ThreadProc,0,0,&theApp.DllThreadID);
  if (theApp.DllThread == NULL) MessageBox(NULL, L"Thread Error", L"Error", MB_OK);
  //else CloseHandle(theApp.DllThread);

  if(m_MsgHWND != 0) theApp.m_HWND_dest = m_MsgHWND;
  if(theApp.OpenPort())
  {
    //if (theApp.DllThread == NULL) theApp.TimerId = SetTimer(NULL, 0, 10, &TimerProc);
    ReadCANVersion();
    return 1;
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SetSleepTime(int value)
{
  if((value > 0) && (value <= 1000))
  {
    theApp.m_sleep_time = (unsigned long)value;
    return 1;
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetEngineTemp()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_enginetemp);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetFuel()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)(theApp.m_fuel * 10));
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetGearboxTemp()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_gearboxtemp);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetAirTemp()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_airtemp);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetSpeed()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    //return(((float)theApp.m_speed_10mph) / 100);
    return((int)theApp.m_speed_10mph);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetRPM()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_rpm);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetBatteryChargeFlag()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_batterycharge);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetWarningEngineTempFlag()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_highenginetemperature);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetWarningOilPressureFlag()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_lowoilpressure);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetCheckEngine()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_checkengine);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetActiveCAN()
{
  int ret_val = -1000;
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    ret_val = (int)theApp.m_canactive;
    return ret_val;
  }
  return ret_val;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetSafetyBeltFlag()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_safetybelt);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetActiveECU()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_ecuactive_fuelconsumption);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetActiveECUFuelConsumption()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_ecuactive);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetActiveSRS()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_srsactive);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetTripDeciMeter()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_trip_dm);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetConsumption10ML()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_consumption_counter_10ml);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SetTripDeciMeter(int value)
{
  theApp.m_trip_dm = value;
  return 1;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SetConsumption10ML(int value)
{
  theApp.m_consumption_counter_10ml = value;
  return 1;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetBreakSwitchState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_break_switch_on);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetRearWindowHeatingState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_rear_window_heating);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetFrontWindowHeatingState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_front_window_heating);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetLowBeamLightState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_low_beam_light);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetHighBeamLightState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_high_beam_light);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetDriversDoorState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_drivers_door_opened);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetPassengersDoorState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_passangers_door_opened);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetRearDoorsState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_rear_doors_opened);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetRightTurnState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_right_turn_light);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetLeftTurnState()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_left_turn_light);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetInCarTemp()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_incartemp);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetTargetTemp()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_targettemp + 13);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetInstConsumption()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_inst_consumption);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetAirFlowDirection()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_air_flow_direction);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetAirFlowSpeed()
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    return((int)theApp.m_air_flow_speed);
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetOBD2ErrorCodes(int n)
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    if(n < MAX_OBD2_CODES)  return((int)theApp.m_obd2_error_code[n]);
    else return -1001;
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) char* GetOBD2ErrorCodeString(int n)
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    if(n < MAX_OBD2_CODES)
    {
      theApp.m_obd2_error_string[5] = 0;
      theApp.m_obd2_error_string[4] = (theApp.m_obd2_error_code[n] & 0xF) | 0x30;
      theApp.m_obd2_error_string[3] = ((theApp.m_obd2_error_code[n] >> 4) & 0xF) | 0x30;
      theApp.m_obd2_error_string[2] = ((theApp.m_obd2_error_code[n] >> 8) & 0xF) | 0x30;
      theApp.m_obd2_error_string[1] = ((theApp.m_obd2_error_code[n] >> 12) & 0x3) | 0x30;
      int code_type = (theApp.m_obd2_error_code[n] >> 14) & 0x3;
      if(code_type == 0) theApp.m_obd2_error_string[0] = 'P';
      else if(code_type == 1) theApp.m_obd2_error_string[0] = 'C';
      else if(code_type == 2) theApp.m_obd2_error_string[0] = 'B';
      else if(code_type == 3) theApp.m_obd2_error_string[0] = 'U';
    }
    else
    {
      theApp.m_obd2_error_string[0] = 0;
      theApp.m_obd2_error_string[1] = 0;
      theApp.m_obd2_error_string[2] = 0;
      theApp.m_obd2_error_string[3] = 0;
      theApp.m_obd2_error_string[4] = 0;
      theApp.m_obd2_error_string[5] = 0;
    }
  }
  return(theApp.m_obd2_error_string);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetOBD2PID(int pid)
{
  if((theApp.hPort != 0) && (theApp.hPort != INVALID_HANDLE_VALUE))
  {
    if(pid < 256)  return((int)theApp.m_obd2_pid_value[pid]);
    else return -1001;
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) char* GetGPSMessage()
{
  return((char*)theApp.m_gps_msg);
}
//*****************************************************************************
extern "C" __declspec(dllexport) char* GetCANVersion()
{
  return((char*)theApp.m_can_version);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetConditionerState()
{
  return((int)theApp.m_conditioner_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetRecirculationState()
{
  return((int)theApp.m_recirculation_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetWindshieldFlowState()
{
  return((int)theApp.m_windshield_flow_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetParkState()
{
  return((int)theApp.m_park_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetMarkerLightState()
{
  return((int)theApp.m_marker_light_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int GetIgnitionState()
{
  return((int)theApp.m_ignition_state);
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ResetECUErrors()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x01;
    theApp.m_output_buf[7] = 0x04;
    theApp.m_output_buf[8] = 0x00;
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x14; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    return 1;
  }
  return 0;
}

//*****************************************************************************
extern "C" __declspec(dllexport) int ResetEngineECU(int mode)
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_reset_ecu_mode = mode & 0xFF;
    theApp.m_resetting_ecu = TRUE;
    theApp.m_resetting_ecu_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int Fan1On()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_fan_switching_on = TRUE;
    theApp.m_fan_switching_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int Fan1Off()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_fan_switching_off = TRUE;
    theApp.m_fan_switching_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int Fan2On()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_fan2_switching_on = TRUE;
    theApp.m_fan2_switching_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int Fan2Off()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_fan2_switching_off = TRUE;
    theApp.m_fan2_switching_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SetIdleRPM(int rpm)
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x06; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_rpm_value = (rpm / 10) & 0xFF;
    theApp.m_setting_idle = TRUE;
    theApp.m_setting_idle_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ResetABSErrors()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE3;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x03; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_clearing_abs_error = TRUE;
    theApp.m_clearing_abs_error_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ResetSRSErrors()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE5;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x01; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_clearing_srs_error = TRUE;
    theApp.m_clearing_srs_error_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ResetTCUErrors()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE1;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x10; // ������ �� �����. ������
    theApp.m_output_buf[8] = 0x01; // ������� ������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0x05; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_clearing_tcu_error = TRUE;
    theApp.m_clearing_tcu_error_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int TesterPresent()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x3E; // Tester present
    theApp.m_output_buf[8] = 0x80; // �� ��������
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFE; // CRC H Byte
    theApp.m_output_buf[15] = 0x59; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ReadCANVersion()
{
  if(!theApp.hPort) return -1000;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0x02;
    theApp.m_output_buf[1] = 0x01;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0;
    theApp.m_output_buf[5] = 0;
    theApp.m_output_buf[6] = 0x01;
    theApp.m_output_buf[7] = 0x01;
    theApp.m_output_buf[8] = 0;
    theApp.m_output_buf[9] = 0;
    theApp.m_output_buf[10] = 0;
    theApp.m_output_buf[11] = 0;
    theApp.m_output_buf[12] = 0;
    theApp.m_output_buf[13] = 0;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    theApp.m_output_buf[15] = 0xFB; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ReadOBD2ErrorCodes(int mode)
{
  if(!theApp.hPort) return -1000;
  if((mode != 3) && (mode != 7)) return -1001;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x01; // ���������� ����
    theApp.m_output_buf[7] = (unsigned char)mode; // ������ ������ ������ OBD2
    theApp.m_output_buf[8] = 0x00;
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    theApp.m_output_buf[14] = 0xFF; // CRC H Byte
    if(mode == 3) theApp.m_output_buf[15] = 0x15; // CRC L Byte
    else if(mode == 7) theApp.m_output_buf[15] = 0x11; // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_reading_errors_obd2 = TRUE;
    theApp.m_reading_errors_obd2_mark = GetTickCount();
    int i;
    for(i = 0; i < MAX_OBD2_CODES; i++) theApp.m_obd2_error_code[i] = 0;
    return 1;
  }
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int ReadOBD2PID(int pid)
{
  if(!theApp.hPort) return -1000;
  if((pid < 0) && (pid > 255)) return -1001;
  if(theApp.m_comsending == FALSE)
  {
    theApp.m_output_buf[0] = 0;
    theApp.m_output_buf[1] = 0;
    theApp.m_output_buf[2] = 0;
    theApp.m_output_buf[3] = 0;
    theApp.m_output_buf[4] = 0x07;
    theApp.m_output_buf[5] = 0xE0;
    theApp.m_output_buf[6] = 0x02; // ���������� ����
    theApp.m_output_buf[7] = 0x01; // ������ ������ pid OBD2
    theApp.m_output_buf[8] = (unsigned char)pid; // ����� pid
    theApp.m_output_buf[9] = 0x00;
    theApp.m_output_buf[10] = 0x00;
    theApp.m_output_buf[11] = 0x00;
    theApp.m_output_buf[12] = 0x00;
    theApp.m_output_buf[13] = 0x00;
    DWORD m_summ = 0;
    DWORD i;
    for(i = 0; i < 14; i++) m_summ += theApp.m_output_buf[i];
    m_summ = 0x10000 - m_summ;
    theApp.m_output_buf[14] = (unsigned char)((m_summ >> 8) & 0xFF); // CRC H Byte
    theApp.m_output_buf[15] = (unsigned char)(m_summ & 0xFF); // CRC L Byte
    theApp.m_output_buf_len = 16;
    theApp.PortWrite();
    theApp.m_reading_pid_obd2 = TRUE;
    theApp.m_reading_pid_obd2_mark = GetTickCount();
    return 1;
  }
  return 0;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SendGPSMessageA(char* msg)
{
  return -1000;
}
//*****************************************************************************
extern "C" __declspec(dllexport) int SendGPSMessageW(WCHAR* msg)
{
  return -1000;
}


